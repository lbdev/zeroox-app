const SECOND = 1000
const MINUTE = 60 * SECOND
const HOUR = 60 * MINUTE
const DAY = 24 * HOUR
export default function parseTimeData(time) {
	const days = Math.floor(time / DAY)
	const hours = Math.floor((time % DAY) / HOUR)
	const minutes = Math.floor((time % HOUR) / MINUTE)
	const seconds = Math.floor((time % MINUTE) / SECOND)
	const milliseconds = Math.floor(time % SECOND)
	return {
		days,
		hours,
		minutes,
		seconds,
		milliseconds
	}
}
