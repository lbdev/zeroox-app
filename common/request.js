import baseUrl from "./baseUrl.js"
import store from '@/store'
import loading from '@/common/custom-loading.js'
import toast from '@/common/custom-toast.js'
import {versionCode} from '@/manifest.json'
var moment = require('moment');

function arrayBufferToBase64(buffer) {
	var binary = '';
	var bytes = new Uint8Array(buffer);
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return window.btoa(binary);
}

var hasNavLogin = false
export function request(options) {
	// console.log("request option", options);
	var showLoading = true;
	if (options.showLoading !== undefined) {
		showLoading = options.showLoading;
	}
	if (showLoading) {
		// uni.showLoading({
		// 	mask: true
		// });
		loading()
	}
	
	const token = uni.getStorageSync('uni_id_token')
	const locale = uni.getStorageSync('locale') || 'en_US'
	const baseConfig = {};
	return new Promise((resolve, reject) => {
		options.url = baseUrl.apiBaseUrl + options.url
		if (!options.data) {
			options.data = {};
		}
		let defaultHeader = {
			'X-Access-Token': token,
			TimeZone:Intl.DateTimeFormat().resolvedOptions().timeZone, //时区
			times:moment().format('YYYY-MM-DD HH:mm:ss')  //当期时间
		}
		if (options.header) {
			options.header = Object.assign(defaultHeader, options.header)
		} else {
			options.header = defaultHeader
		}
		options.timeout = 60000
		options.success = res => {
			// console.log('res', res)
			if (showLoading) {
				loading(false)
			}
			if (res.statusCode >= 200 && res.statusCode < 300) {
				hasNavLogin = false
				// 弹出成功请求
				if (options.succNotice) {
					uni.showToast({
						icon: 'none',
						title: res.data.message || '请求成功!'
					})
				}
				if (res.data.code == 200 || res.data.code == 0) {
					resolve(res.data.result)
					return
				}
				if (res.data.code == 50000 || res.data.code == 401) {
					store.commit('user/logout')
					uni.$emit('requestLogin')
					uni.reLaunch({
						url: "/pages/ucenter/login-page/index/index"
					})
					if (showLoading) {
						loading(false)
					}
					reject(res.data)
					return
				}
				if (showLoading) {
					loading(false)
				}
				// uni.showToast({
				// 	icon: 'none',
				// 	duration: 3000,
				// 	title: res.data.message || res.data.msg || 'Unknow Error!'
				// });
				toast(res.data.message || res.data.msg || 'The server is busy now, please try again later!', 'long')
				reject(res.data)
				return
			}
			if (res.statusCode == 401) {
				store.commit('user/logout')
				uni.$emit('requestLogin')
				uni.reLaunch({
					url: "/pages/ucenter/login-page/index/index"
				})
				if (showLoading) {
					loading(false)
				}
				reject(res.data)
				return
			}
			if (showLoading) {
				loading(false)
			}
			// 网络状态错误的情况
			// uni.showToast({
			// 	icon: 'none',
			// 	duration: 3000,
			// 	title: res.data.message || 'The server is a little tired, please try again later!'
			// });
			toast(res.data.message || 'The server is busy now, please try again later!')
			reject(res)
		}
		options.fail = err => {
			if (showLoading) {
				// uni.hideLoading()
				loading(false)
			}
			uni.showToast({
				icon: 'none',
				duration: 3000,
				title: err.message || 'Please check the network!'
			});
			reject(err);
		}
		options.complete = () => {
			// console.log('request completed')
		}
		// console.log('req', options.url, JSON.stringify(options.data))
		return uni.request(Object.assign({}, baseConfig, options))
	})
};

export function download(options) {
	const token = uni.getStorageSync('token')
	const baseConfig = {};
	return new Promise((resolve, reject) => {
		if (!options.url) {
			reject(I18n.t('global.reqURL'))
		};
		if (!token && APIs_NOT_AUTH.indexOf(options.url) == -1) {
			setTimeout(() => {
				uni.navigateTo({
					url: '/pages/login/login'
				});
			}, 500)
			reject(I18n.t('global.notLogin'))
			return
		}
		if (options.baseUrl) {
			options.url = options.baseUrl + options.url
		} else {
			options.url = BASE_URL + options.url
		}
		options.success = res => {
			// console.log(`response: ${JSON.stringify(res)}`);
			// uni.hideLoading();
			loading()
			if (res.statusCode >= 200 && res.statusCode < 300) {
				resolve(res.tempFilePath);
				return;
			} else if (res.statusCode == 401) {
				uni.showToast({
					icon: 'none',
					title: I18n.t('global.overdue')
				});
				setTimeout(function() {
					uni.navigateTo({
						url: '/pages/login/login'
					});
				}, 3000)
				reject(I18n.t('global.overdue'))
				return
			}
			// 网络状态错误的情况
			uni.showToast({
				icon: 'none',
				duration: 3000,
				title: res.statusText || I18n.t('global.retry')
			});
			reject(res.statusText || I18n.t('global.retry'));
		}
		options.fail = err => {
			// 关闭loading
			// console.log(`request error.${JSON.stringify(err)}`);
			uni.showToast({
				icon: 'none',
				duration: 5000,
				title: I18n.t('global.timeout')
			});
			reject(err.errMsg);
		}
		options.complete = () => {
			console.log('request completed')
		}
		baseConfig.header = {
			'content-type': options.contentType ? options.contentType : 'application/json',
			'client_id': 'app',
			'client_secret': 'app',
			'Authorization': 'bearer ' + token
		}
		let showLoading = true;
		if (options.showLoading !== undefined) {
			showLoading = options.showLoading;
		}
		if (showLoading && requestCount == 0) {
			// uni.showLoading({
			// 	title: options.loadingText || I18n.t('global.loading'),
			// 	mask: true
			// });
			loading()
		}
		// console.log(`request: ${JSON.stringify(options)}`);
		return uni.downloadFile(Object.assign({}, baseConfig, options))
	})
}
