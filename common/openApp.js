/*
	创建在h5端全局悬浮引导用户下载app的功能，
	如不需要本功能直接移除配置文件uni-starter.config.js下的h5/openApp即可
*/

import CONFIG from '../uni-starter.config.js';
import i18n from '@/lang/index.js'

const CONFIG_OPEN = CONFIG?.h5?.openApp?.openUrl ? CONFIG.h5.openApp : {};
// 仅H5端添加"打开APP"
export default function() {
	// #ifdef H5
	let openApp = document.createElement("div");
	let btnClose = document.createElement("span");
	let text = document.createElement("div");
	
	openApp.id = 'openApp';
	btnClose.id = 'btnClose';
	text.id='text';
	text.innerText='';
	openApp.appendChild(btnClose);
	openApp.appendChild(text);
	
	openApp.style =
		'position: fixed;top: 0px;left: 43px;right: 0;z-index: 99;width: 85px;height: 40px;background-image: url(/static/download_logo.png);background-size: 100% 100%;background-repeat: no-repeat;background-position-x: center;text-align: center;'
	btnClose.style =
		'width:16px;height:16px;  display: block;position: absolute;right: 0; bottom: 0'
	text.style='color:#ffffff;text-align: center;font-size: 10px;margin-top: 2px;overflow:hiden;max-width: 50px;display: inline-block;display: inline-block;white-space: nowrap;overflow: hidden;text-overflow:ellipsis;'
	document.body.insertBefore(openApp, document.body.firstChild);
	// document.body.style = 'height:calc(100% - 45px);';
	openApp.addEventListener('click', e => {
		// var target = e.target || e.srcElement;
		// if (target.className.indexOf('openBtn') >= 0) {
		window.location.href = CONFIG_OPEN.openUrl;
		// }
	})
	btnClose.addEventListener('click', e => {
		openApp.remove();
		e.stopPropagation();

	})
	//#endif
}
