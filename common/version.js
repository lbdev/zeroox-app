import {env} from '@/common/build-env.js'
let versionList
if (env == 'dev' || process.env.NODE_ENV === 'development') {
	versionList = [{
		code: 'PE',
		countryName: '索尔',
		domainUrl: 'http://123.57.18.182:9600',
		wsDomainUrl: 'ws://18.167.126.62:58080/subscribOrder',
		number: 51,
		contacts: [{
			type: 'telegram',
			url: 'https://t.me/IEARNBOT_official'
		}]
	}, {
		code: 'NG',
		countryName: '奈拉',
		domainUrl: 'http://8.141.164.247:9600',
		wsDomainUrl: 'ws://18.167.126.62:58081/subscribOrder',
		number: 234,
		contacts: [{
			type: 'telegram',
			url: 'https://t.me/IEARNBOT_VIP'
		}, {
			type: 'telegram',
			url: 'https://t.me/IEARNBOTOfficial'
		}]
	}, {
		code: 'US',
		countryName: '美元',
		domainUrl: 'http://8.141.164.247:9700',
		wsDomainUrl: 'ws://18.167.126.62:58082/subscribOrder',
		number: 1,
		contacts: [{
			type: 'telegram',
			url: 'https://t.me/IEARNBOTCS'
		}, {
			type: 'telegram',
			url: 'https://t.me/IEARNBOT_CS'
		}]
	}]
} else {
	versionList = [{
		code: 'PE',
		countryName: '索尔',
		domainUrl: 'https://api.iearnbot.com',
		wsDomainUrl: 'wss://ws.iearnbot.com/subscribOrder',
		number: 51,
		contacts: [{
			type: 'telegram',
			url: 'https://t.me/IEARNBOT_official'
		}]
	}, {
		code: 'NG',
		countryName: '奈拉',
		domainUrl: 'https://api.iearnbot.com/ng',
		wsDomainUrl: 'wss://ws.iearnbot.com/subscribOrder',
		number: 234,
		contacts: [{
			type: 'telegram',
			url: 'https://t.me/IEARNBOT_VIP'
		}, {
			type: 'telegram',
			url: 'https://t.me/IEARNBOTOfficial'
		}]
	}, {
		code: 'US',
		countryName: '美元',
		domainUrl: 'https://api.iearnbot.com/en',
		wsDomainUrl: 'wss://ws.iearnbot.com/subscribOrder',
		number: 1,
		contacts: [{
			type: 'telegram',
			url: 'https://t.me/IEARNBOTCS'
		}, {
			type: 'telegram',
			url: 'https://t.me/IEARNBOT_CS'
		}]
	}]
}
export default versionList
