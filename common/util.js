//#ifdef H5
export const browser = {
	versions: function() {
		var u = navigator.userAgent,
			app = navigator.appVersion;
		var data = { //移动终端浏览器版本信息
			trident: u.indexOf('Trident') > -1, //IE内核
			presto: u.indexOf('Presto') > -1, //opera内核
			webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
			gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
			ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
			android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
			iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
			iPad: u.indexOf('iPad') > -1, //是否iPad
			webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
		}
		data.mobile = data.ios || data.android || data.iPad || data.iPhone
		return data;
	}(),
	language: (navigator.browserLanguage || navigator.language).toLowerCase()
}
//#endif
//h5复制文本到剪切板
export function copy({
	text,
	success,
	fail
}) {
	//#ifdef H5
	var copyDom = document.createElement('div');
	copyDom.innerText = text;
	copyDom.style.position = 'absolute';
	copyDom.style.top = '0px';
	copyDom.style.right = '-9999px';
	document.body.appendChild(copyDom);
	//创建选中范围
	var range = document.createRange();
	range.selectNode(copyDom);
	//移除剪切板中内容
	window.getSelection().removeAllRanges();
	//添加新的内容到剪切板
	window.getSelection().addRange(range);
	//复制
	try {
		document.execCommand('copy');
		if (success) {
			success();
		}
		console.log('copy success')
	} catch (err) {
		if (fail) {
			fail(err);
		}
		console.log('copy failed')
	} finally {
		copyDom.parentNode.removeChild(copyDom);
	}
	//#endif
	//#ifndef H5
	uni.setClipboardData({
		data: text,
		success,
		showToast: false,
		fail
	})
	//#endif
}
export function getQueryParam(name) {
	//从索引1处开始截取，即舍弃掉"？"
	let search = location.search.substring(1)	
	
	//通过正则表达式匹配，例如 ： a=10&b=70&password=90
	//注意看下面的正则表达式:
	//	（一）、(^|&) 匹配的是开始或者是&，如上a前面的空字符开始，和b前面的&符号
	//	（二）、${name} 是一个模板字符串，匹配的是我们传递进来的name
	//	（三）、([^&]*)第二个分组指定匹配的字符不能是&的一个或多个字符，如上，匹配的是10,70,90，但是仅这样还不行，还没有结尾
	//	（四）、(&|$)第四个分组指定我们查询参数的结尾，是以&结尾或者是结尾，没有这个分组，我们的参数结不了尾。 比如a=10这里以&结尾，匹配到10；b=70匹配到70，以&结尾；password=90匹配到是是字符的结尾，后面没有内容，所以匹配到90
		
	let reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`)
	let result = search.match(reg)
	if(result == null){
		return null
	}else{
		//这里之所以是索引2，是因为上面的正则表达式中我们真正要返回的查询参数值是第2对小括号中的内容，索引0处返回的是整个匹配的正则表达式对象
		return result[2]
	}
}
function typeJudge(item) {
	if (Object.prototype.toString.call(item) === '[object Array]') {
		return 'Array';
	} else if (Object.prototype.toString.call(item) === '[object Object]') {
		return 'Object';
	} else {
		return 'Basic';
	}

}

export function objectFlat(obj = {}) {
	let res = {};

	function flat(item, preKey = '') {
		if (typeJudge(item) == 'Object') {
			for (let key in item) {
				if (typeJudge(item[key]) != 'Basic') {
					flat(item[key], preKey == '' ? `${key}` : `${preKey}.${key}`)
				} else {
					if (preKey == '') {
						res[`${key}`] = item[key];
					} else {
						res[`${preKey}.${key}`] = item[key];
					}
				}
			}
		} else if (typeJudge(item) == 'Array') {
			for (let key in item) {
				if (typeJudge(item[key]) != 'Basic') {
					flat(item[key], preKey == '' ? `${key}` : `${preKey}[${key}]`)
				} else {
					if (preKey == '') {
						res[`${key}`] = item[key];
					} else {
						res[`${preKey}[${key}]`] = item[key];
					}
				}
			}
		}
	}
	flat(obj)
	return res;
}


/*
根据后缀名判断文件类型
*/
export function getFileType(type) {
	const imglist = ['png', 'jpg', 'jpeg', 'bmp', 'gif']
	// 匹配 视频
	const videolist = [
		'mp4',
		'm2v',
		'mkv',
		'rmvb',
		'wmv',
		'avi',
		'flv',
		'mov',
		'm4v'
	]
	if (imglist.includes(type)) {
		return 'image'
	} else
	if (videolist.includes(type)) {
		return 'video'
	} else {
		return 'file'
	}
}
