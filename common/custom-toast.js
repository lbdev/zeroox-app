export default (text, duration = 'short') => {
	// #ifdef APP
	plus.nativeUI.toast(`<font color="#fff">${text}</font>`, {
		duration,
		align: 'center',
		verticalAlign: 'center',
		background: 'rgba(0, 0, 0, 0.6)',
		type: 'richtext'
	});
	// #endif
	// #ifdef H5
	return new Promise((resolve => {
		uni.showToast({
			title: text,
			duration: 1500,
			icon: "none"
		})
		setTimeout(() => {
			resolve()
		}, 1500)
	}))
	// #endif
}
