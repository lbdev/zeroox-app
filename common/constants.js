import Vue from "vue"
export const orderStatusDic = {
	'Create': {
		text: 'Create',
		value: 'Create',
		color: '#D1523D'
	},
	'Under Review': {
		text: 'Under Review',
		value: 'Under Review',
		color: '#EFA941'
	},
	'Reject': {
		text: 'Reject',
		value: 'Reject',
		color: '#D1523D'
	},
	'Loaning': {
		text: 'Loaning',
		value: 'Loaning',
		color: '#A844BC'
	},
	'Overdue': {
		text: 'Overdue',
		value: 'Overdue',
		color: '#FC795F'
	},
	'Cleared': {
		text: 'Cleared',
		value: 'Cleared',
		color: '#37CBC1'
	},
	'To Be Paid': {
		text: 'To Be Paid',
		value: 'To Be Paid',
		color: '#3C7CE9'
	}
}