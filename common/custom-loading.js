var loading = false
export default (show = true) => {
	// #ifdef APP
	if (show) {
		if (loading) {
			return
		}
		loading = true
		plus.nativeUI.showWaiting('', {
			background: 'rgba(0,0,0,0)',
			loading: {
				display: 'block',
				icon: 'static/loading.png',
				height: 30,
				interval: 100
			}
		})
	} else {
		plus.nativeUI.closeWaiting()
		loading = false
	}
	
	// #endif
	// #ifdef H5
	let customLoading = document.getElementById('custom-loading')
	if (!customLoading) {
		customLoading = document.createElement("div")
		customLoading.id = 'custom-loading'
		customLoading.style = `
			position: fixed;
			left: 0;
			right: 0;
			bottom:0;
			top: 0;
			background-color:#00000005;
			display: flex;
			justify-content: center;
			align-items: center;
			z-index: 100000;
		`
		customLoading.innerHTML = `
			<img src="/static/loading-primary.png" style="
				width: 30px;
				height: 30px;
				-webkit-animation: spin 1s linear 1s 5 alternate;
				animation: spin 1s linear infinite;" />
		`
		document.body.insertBefore(customLoading, document.body.firstChild);
	}
	customLoading.style.display = show ? 'flex' : 'none'
	// #endif
}
