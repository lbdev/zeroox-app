import {
    env
} from '@/common/build-env.js'
let apiBaseUrl, h5BaseUrl,wsBaseUrl,stripeKey

if (env == 'dev' || process.env.NODE_ENV === 'development') {
	apiBaseUrl = 'http://zeroox.lbdev.cssoftstudio.cn/api/'
	wsBaseUrl = 'ws://zeroox.lbdev.cssoftstudio.cn/api/websocket/'
    h5BaseUrl = 'http://zeroox.lbdev.cssoftstudio.cn'
	//订阅环境测试
	stripeKey='pk_test_51LIvQWCSrBuzWQejwe61Z0HhPQ9Y1uDu7L8OXhzHS3TDKq94LwNKGjt4TZnzwW0BGlgOenYKgaVDjov097N5g2Ln00neFCLerg'
} else {
	apiBaseUrl = 'https://urzeroox.com'
	wsBaseUrl = 'wss://urzeroox.com/api/websocket/'
    h5BaseUrl = 'https://app.urzeroox.com'
	//订阅环境测试
	stripeKey='pk_live_51LIvQWCSrBuzWQejWj14itcnWXqNHKi42y8RVdXGV9jH8Zkq35n2plGWxIyfxakOBEP6GAMt0TKHo4vmQz9Hqz9U00EzNFCe4g'
}
export default {
    apiBaseUrl,
    h5BaseUrl,
	wsBaseUrl,
	stripeKey
}