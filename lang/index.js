import lang_en_US from "@/lang/en_US.js"
// import lang_es_ES from "@/lang/es_ES.js"
// import lang_zh_CN from "@/lang/zh_CN.js"
	// // #ifdef APP-PLUS
	// console.log('xxxxxxxxxxxxxxx', plus.os.language)
	// // #endif
	// // #ifndef APP-PLUS
	// console.log('xxxxxxxxxxxxxxx', navigator.language)
	// // #endif

import VueI18n from 'vue-i18n'
import Vue from 'vue'
Vue.use(VueI18n)
let messages = {
	'en_US': lang_en_US
}
let locale = uni.getStorageSync('locale') || 'en_US'
let langData = uni.getStorageSync(locale)
if (langData) {
	messages[locale] = langData
}
const i18n = new VueI18n({
	locale, // 设置地区
	messages
})
export default i18n
