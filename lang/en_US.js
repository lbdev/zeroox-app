export default {
	"login": {
		"welcome": "Welcome!",
		"continue": "Sign in to continue",
		"login": "LOGIN",
		"sign_up": "SIGN UP",
		"realname": "Full Name",
		"email": "Email",
		"password": "Password",
		"forgot_password": "Forgot password ?",
		"login_google": "Login With Google",
		"signUp_google": "Sign Up With Google",
		"text1": "By signing up you agree to",
		"text2": "T&Cs",
		"text3": "and",
		"text4": "Privacy Policy",
		"realname_tip": "Please enter your full name",
		"email_tip": "Please fill in the email",
		"password_tip": "Please enter your password",
		"passwordInvalid": "The password must contain at least 8 characters, including uppercase, lowercase and numbers."
	}
}
