import {
	checkVersion,
	getAuthStatus
} from "@/api/user.js"
let state = {
		appName: uni.getStorageSync('appName') || 'Zeroox',
		appVersion: uni.getStorageSync('appVersion') || '1.0.0',
		versionInfo: {
			newVersion: '1.0.0',
			minVersion: '1.0.0'
		},
	},
	getters = {

	},
	mutations = {
		setAppName(state, appName) {
			state.appName = appName;
			uni.setStorageSync('appName', appName);
		},
		setAppVersion(state, version) {
			state.appVersion = version;
			uni.setStorageSync('appVersion', version);
		},
		setVersionInfo(state, versionInfo) {
			state.versionInfo = versionInfo
		}
	},
	actions = {
		async init(store) {
			const { commit, dispatch, state, rootState } = store
			try {
				
			} catch (e) {
				console.error(e)
			}
		}
	}
export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}
