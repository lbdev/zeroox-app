// 上次启动时的用户信息
let userInfoHistory = uni.getStorageSync('userInfo') || {};
	// import {
	// 	getFailState
	// } from '@/api/subscription.js'
let state = {
		//是否已经登录
		token: uni.getStorageSync('uni_id_token') || '',
		tokenExpired: (uni.getStorageSync('uni_id_token_expired') || 0) < Date.now(),
		hasLogin: Boolean(Object.keys(userInfoHistory).length),
		//用户信息
		info: userInfoHistory
	},
	getters = {
		info(state) {
			return state.info;
		},
		hasLogin(state) {
			return state.hasLogin;
		},
	},
	mutations = {
		login(state, info) { //登录成功后的操作
			//原有的结合传来的参数
			console.log('@@@@', info)
			let _info = state.info;
			state.info = Object.assign({}, _info, info);
			//设置为已经登录
			state.hasLogin = true;
			//存储最新的用户数据到本地持久化存储
			state.token = state.info.token;
			state.tokenExpired = state.info.tokenExpired;
			uni.setStorageSync('userInfo', state.info);
			uni.setStorageSync('uni_id_token', state.info.token)
			uni.setStorageSync('uni_id_token_expired', state.info.tokenExpired)
		},
		updateUserInfo(state, info) {
			let _info = state.info;
			state.info = Object.assign({}, _info, info);
			uni.setStorageSync('userInfo', state.info);
		},
		logout(state) {
			state.info = {};
			state.hasLogin = false;
			uni.setStorageSync('userInfo', {});
			uni.setStorageSync('uni_id_token', '');
			uni.setStorageSync('uni_id_token_expired', 0)
		},
		setAuthStatus(state, status) {
			state.authStatus = status
			uni.setStorageSync('authStatus', status)
		},
		// setFailState(state,value){
		// 	state.info.failState=value
		// 	uni.setStorageSync('userInfo', state.info);
		// 	// uni.setStorageSync('failState', value)
		// }
	},
	actions = {
		// async getState(context) {
		// 	if(state.info.userType==1){
		// 		return
		// 	}
		// 	try {
		// 		let res = await getFailState({
		// 			id:this.state.user.info.id
		// 		})
		// 		context.commit('setFailState',res)
		// 		console.log('状态@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@', res)
		// 	} catch (e) {
		// 		console.error(e)
		// 	}
		// }
	}
export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}
