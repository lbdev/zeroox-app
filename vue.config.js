const ignored = ['**/uni_modules/**/*.md', '**/uni_modules/**/package.json', '**/uni_modules/*/uniCloud/**/*']
const CopyPlugin = require("copy-webpack-plugin");
const path = require('path');
module.exports = {
	configureWebpack() {
		return {
			watchOptions: {
				ignored
			},
			devServer: {
				// proxy: {
				// 	"/api": {
				// 		target: "http://1.15.24.5:3007/api",
				// 		changeOrigin: true,
				// 		ws: true,
				// 		secure: false,
				// 	},
				// },
				watchOptions: {
					ignored
				}
			},
			plugins: [
				new CopyPlugin([{
						from: path.join(__dirname, '/sw.js'),
						to: path.join(__dirname, 'unpackage/dist', process.env.NODE_ENV === 'production' ?
							'build' : 'dev', process.env.UNI_PLATFORM, '')
					},
					{
						from: path.join(__dirname, '/index-pc.html'),
						to: path.join(__dirname, 'unpackage/dist', process.env.NODE_ENV === 'production' ?
							'build' : 'dev', process.env.UNI_PLATFORM, '')
					},
					{
						from: path.join(__dirname, '/manifest.webmanifest'),
						to: path.join(__dirname, 'unpackage/dist', process.env.NODE_ENV === 'production' ?
							'build' : 'dev', process.env.UNI_PLATFORM, '')
					}
				])
			]
		}
	}
}
