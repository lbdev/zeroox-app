import {
	request
} from "@/common/request.js"
// 获取当前时间
export function getGoogleLogin({
	showLoading = true,
}) {
	return request({
		showLoading,
		url: '/api/sys/google/login',
		method: 'get'
	})
}

//通过谷歌code获取用户token
export function getTokenByCode({
	showLoading = true,
	code
}) {
	return request({
		showLoading,
		url: '/api/sys/google/result',
		method: 'get',
		data: {
			code
		}
	})
}
//谷歌登录
export function googleRegister({
	showLoading = true,
	data = {
		email, //邮箱
		hmo, //是否有邮箱
		houseName, //房屋名称
		houseQuantity, //房主或房东输入房子数量
		houseType, //房屋类型
		invite, //是否邀请
		inviteId, //邀请人id
		landlordEmail, //房东输入租客邮箱
		landlordName, //	房东输入租客名称
		password, //用户密码
		userType, //用户角色类型,tenant,landlord,homeowner
		realname, //用户账号名
		hearSource,//来源
		referee,//自定义来源值
		uniqueLink //推广码
	}
}) {
	data.uniqueLink=localStorage.getItem("uniqueLink")
	console.log('uniqueLink',data.uniqueLink)
	return request({
		url: '/api/sys/google/userInfoPerfect',
		method: 'post',
		data: {
			...data
		}
	})
}
//注册
export function register({
	showLoading = true,
	data = {
		email, //邮箱
		hmo, //是否有邮箱
		houseName, //房屋名称
		houseQuantity, //房主或房东输入房子数量
		houseType, //房屋类型
		invite, //是否邀请
		inviteId, //邀请人id
		landlordEmail, //房东输入租客邮箱
		landlordName, //	房东输入租客名称
		password, //用户密码
		userType, //用户角色类型,tenant,landlord,homeowner
		realname, //	用户账号名
		hearSource,//来源
		referee,//自定义来源值
		uniqueLink//推广码
	}
}) {
	data.uniqueLink=localStorage.getItem("uniqueLink")
	console.log('uniqueLink',data.uniqueLink)
	return request({
		url: '/api/sys/user/enrollMailbox',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}

//登录
export function loginMailbox({
	showLoading = true,
	data = {
		email, //用户邮箱
		password //	用户密码
	}
}) {
	return request({
		url: '/api/sys/loginMailbox',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}
//默认展示的用户首页快捷菜单接口
export function hostHomePage({
	showLoading = true,
	// data = {
	// 	userId
	// }
}) {
	return request({
		url: '/api/home/hostHomePage',
		showLoading,
		method: 'post',
		// data: {
		// 	...data
		// }
	})
}

// 用户可添加的首页快捷菜单接口(列表)
export function hostHomeShort({
	showLoading = true,
	// data = {
	// 	userId
	// }
}) {
	return request({
		url: '/api/home/hostHomeShort',
		showLoading,
		method: 'post'
		// data: {
		// 	...data
		// }
	})
}
// 用户添加首页快捷菜单接口
export function addUserShort({
	showLoading = true,
	data = {
		actionId, //首页快捷菜单id
		userId //用户id
	}
}) {
	return request({
		url: '/api/home/addUserShort',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
// 用户删除首页快捷菜单接口
export function romveUserShort({
	showLoading = true,
	data = {
		actionId, //首页快捷菜单id
	}
}) {
	return request({
		url: '/api/home/romveUserShort',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}

//获取维修一级菜单
export function repairDictionaryAll({
	showLoading = true,
	data = {
		code: null, //菜单编号 不传参（查询所有一级子类维修问题）传参（查询层级子类维修问题）
	}
}) {
	return request({
		url: '/api/maintenance/repairDictionaryAll',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}


//忘记密码发送邮箱
export function sendEmail({
	showLoading = true,
	email
}) {
	return request({
		url: '/api/sys/userEmail',
		showLoading,
		method: 'get',
		data: {
			email
		}
	})
}

//重置密码的接口
export function resetPasswords({
	showLoading = true,
	password,
	token
}) {
	return request({
		url: '/api/sys/forgotPassword',
		showLoading,
		method: 'post',
		data: {
			password,
			token
		}
	})
}
//获取维修人员列表
export function getRepairList({
	showLoading = true
}) {
	return request({
		url: '/api/zeroox/zerooxContractor/selcet',
		showLoading,
		method: 'get',
	})
}
//获取房子列表
export function getHouseList({
	showLoading = true,
	keyword
}) {
	return request({
		url: '/api/maintenance/propertyList',
		showLoading,
		method: 'get',
		data: {
			keyword
		}
	})
}
// 发布维修需求(租客)
export function tenantMaintenance({
	showLoading = true,
	data = {
		title, //（维修标题）
		detail, //（维修问题详情内容）
		images: null,
		videos: null,
		times, //上门时间
		fileSize
	}
}) {
	return request({
		url: '/api/maintenance/tenantsRequest',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}
// 发布维修需求（房东或房主）并安排维修人员接口
export function landlordRequest({
	showLoading = true,
	data = {
		title, //（维修标题）
		detail, //（维修内容）
		times, //（上门时间）
		maintenanceManId, //（ 维修人id）
		propertyId, //（房子id）
		contactId, //联系人ID
		contactName, //（联系人）
		contactPhone, //（联系人电话）
		floor, //（楼层数）
		lift, //（是否有电梯）
		paking, //（停车收费）
		status, //（维修状态）
		repairBySelf, //是否是本人
		fileSize
	}
}) {
	return request({
		url: '/api/maintenance/request',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}

// 通过传target（邮箱）获取后台Token
export function createConversation({
	showLoading = true,
	data = {
		target
	}
}) {
	return request({
		url: '/api/chat/twilio/createConversation',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}

// 获取所有租户列表信息
export function getTenantList({
	showLoading = true,
	data = {
		propertyId, //房子id
		name, //房客名
		email, //房客邮箱
		createTime: null, //创建时间
		createBy, //（创建人）
		updateBy, //更新人
		updateTime //更新时间
	}
}) {
	return request({
		url: '/room/tenant/getTenantList',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}

// 获取联系人列表
export function contacListAPI({
	showLoading = true,
	propertyId
}) {
	return request({
		url: '/api/maintenance/contacList',
		showLoading,
		method: 'get',
		data: {
			propertyId
		}
	})
}


//获取请求列表页面
export function getRequestList({
	showLoading = true,
	data = {
		pageNum,
		pageSize,
		status,
		keyword
	}
}) {
	return request({
		url: '/api/maintenance/pendingPage',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//获取取消维修列表
export function getCanceJobList({
	showLoading = true,
	data = {
		pageNum,
		pageSize,
		keyword
	}
}) {
	return request({
		url: '/api/maintenance/canceJob',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//获取维修工作页面详情数据
export function getrepairWorkDetailsList({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/maintenance/repairWorkDetails',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//房主房东从新安排维修时间
export function againRepairTime({
	showLoading = true,
	data = {
		id,
		time
	}
}) {
	return request({
		url: '/api/maintenance/repairTime',
		showLoading,
		method: 'put',
		data: {
			...data
		}
	})
}
//修改状态
export function modifyStatus({
	showLoading = true,
	data = {
		id,
		status
	}
}) {
	return request({
		url: '/api/maintenance/modifyStatus',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//租客对维修人员评价
export function assessRepairPersonnel({
	showLoading = true,
	data = {
		maintenanceId, //维修id
		communicationScore, //对维修人员字段
		attendanceScore, //对维修人员字段
		respectfulScore, //对维修人员字段
		workmanshipScore, //对维修人员字段
		images,
		videos,
		remarks
	}
}) {
	return request({
		url: '/api/maintenance/maintenanceEvaluation',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}

// 取消维修需求
export function cancel_repair({
	showLoading = true,
	data = {
		id,
		statusName
	}
}) {
	return request({
		url: '/api/maintenance/cancelJob',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}

// 显示维修日志
export function showStatusNotes({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/maintenance/statusNotes',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
// 添加维修日志
export function addTenantNotes({
	showLoading = true,
	data = {
		maintenanceId,
		content,
		imagesOrVideo,
		fileSize
	}
}) {
	return request({
		url: '/api/maintenance/tenantNotes',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}
//安排时间
export function repairTime({
	showLoading = true,
	data = {
		id,
		time
	}
}) {
	return request({
		url: '/api/maintenance/repairTime',
		showLoading,
		method: 'put',
		data: {
			...data
		}
	})
}
//安排维修人员 重新安排维修人员
export function arrangeMaintenance({
	showLoading = true,
	data = {
		id, //维修请求id
		urgent, //是否申请紧急救援
		maintenanceManId, //维修人id
		propertyId, //房子id
		contactName, //联系人名字
		contactPhone, //联系人电话
		contactId, //联系人id
		floor, //楼层数
		lift, //是否有电梯,true为有，false没有,默认为有
		parking, //停车收费
		sendEmail, //是否发送邮件
		sendMessage, //是否发送短信
		email, //维修人员邮箱
		phone, //维修人员手机号
		repairBySelf //是否房东自己维修
	}
}) {
	return request({
		url: '/api/maintenance/arrangement',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}
//通知中心
export function notifications({
	showLoading = true,
	data = {
		pageNum,
		pageSize
	}
}) {
	return request({
		url: '/api/maintenance/notifications',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//修改通知中心状态
export function updateNotification({
	showLoading = true,
}) {
	return request({
		url: '/api/maintenance/updateNotification',
		showLoading,
		method: 'put',
	})
}
//获取首页通知数量的接口
export function getHomeNoticeQuantity({
	showLoading = true
}) {
	return request({
		url: '/api/maintenance/homeNotice',
		showLoading,
		method: 'get',
	})
}

//退出登录
export function loginOut({
	showLoading = true
}) {
	return request({
		url: '/api/sys/loginOut',
		showLoading,
		method: 'get',
	})
}
//修改密码接口
export function changePassword({
	showLoading = true,
	data = {
		oldPassword,
		newPassword
	}
}) {
	return request({
		url: '/api/sys/changePassword',
		showLoading,
		method: 'put',
		data: {
			...data
		}
	})
}
//修改名字电话
export function updateNamePhone({
	showLoading = true,
	data = {
		userName,
		userPhone,
		avatar
	}
}) {
	return request({
		url: '/api/sys/nameAndPhoneNumber',
		showLoading,
		method: 'put',
		data: {
			...data
		}
	})
}

//删除用户接口
export function deletUser({
	showLoading = true,
	radioContent,
	code
}) {
	return request({
		url: '/api/sys/deletUser',
		showLoading,
		method: 'delete',
		data: {
			radioContent,
			code
		}
	})
}

//用户反馈二级选项接口
export function getSuggestionTypeList({
	showLoading = true,
	typeName
}) {
	return request({
		url: '/api/suggestion/zerooxUserSuggestion/suggestionType',
		showLoading,
		method: 'get',
		data: {
			typeName
		}
	})
}
//用户反馈
export function submitFeedback({
	showLoading = true,
	data = {
		category, //反馈类型
		tenancyAmount, //租约数量
		toolManagement, //工具管理
		message, //反馈描述
		attachments, //文件
		os, //系统信息
		source, //来源信息
		token, //用户token
		fileSize //文件大小
	}
}) {
	return request({
		url: '/api/suggestion/zerooxUserSuggestion/add',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}
//通过title获取隐私政策富文本（Privacy Policy，TCs，Cookie）
export function getDetailForTitle({
	title,
	showLoading = true
}) {
	return request({
		url: '/api/text/pageText/queryByTitle',
		showLoading,
		method: 'get',
		data: {
			title
		}
	})
}

//首页判断账号是否重复注册
export function verifyMailbox({
	email,
	showLoading = true
}) {
	return request({
		url: '/api/sys/user/existsEmail',
		showLoading,
		method: 'get',
		data: {
			email
		}
	})
}


//回显
export function getEvaluation({
	evaluationId,
	showLoading = true
}) {
	return request({
		url: '/api/maintenance/evaluationPage',
		showLoading,
		method: 'get',
		data: {
			evaluationId
		}
	})
}


//判断房东是否删除了租约
export function isDeleteZerooxTenancy({
	showLoading = true
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/isDeleteZerooxTenancy',
		showLoading,
		method: 'get'
	})
}

//添加完成报告
export function addReport({
	showLoading = true,
	data = {
		// id,//报告id
		userId, //用户id
		requestId, //维修需求id
		achievementTime, //完成时间
		maintenanceManId, //维修人员id
		workContent, //描述工作内容
		contentImages, //工作图片
		notes, //备注
		notesImages, //备注图片
		fileSize //文件大小
	}
}) {
	return request({
		url: '/api/zerooxCompletionReportController/add',
		showLoading,
		method: 'post',
		data: {
			...data
		}
	})
}
//查看完成报告
export function getReport({
	showLoading = true,
	requestId
}) {
	return request({
		url: '/api/zerooxCompletionReportController/query',
		showLoading,
		method: 'get',
		data: {
			requestId
		}
	})
}
//首页统计评论的新消息提醒
export function getRewcount({
	showLoading = true
}) {
	return request({
		url: '/api/home/rewcount',
		showLoading,
		method: 'get'
	})
}

//获取删除用户验证码
export function getVerificationCode({
	showLoading = true
}) {
	return request({
		url: '/api/sys/sendVerificationCode',
		showLoading,
		method: 'get'
	})
}


//添加花销
export function addExpenseApi({
	showLoading = true,
	data
}) {
	return request({
		url: '/api/expense/addExpense',
		showLoading,
		method: 'post',
		data
	})
}
// 根据需求id分页查询花销列表
export function getExpenseListApi({
	showLoading = true,
	pageNum,
	pageSize,
	requestId
}) {
	return request({
		url: '/api/expense/queryRequestId',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			requestId
		}
	})
}

// 根据花销id查询详情
export function getExpenseDetailApi({
	showLoading = true,
	expenseId
}) {
	return request({
		url: '/api/expense/queryId',
		showLoading,
		method: 'get',
		data: {
			expenseId
		}
	})
}
// 根据花销id编辑花销
export function updateExpenseDetailApi({
	showLoading = true,
	data
}) {
	return request({
		url: '/api/expense/updateExpense',
		showLoading,
		method: 'post',
		data
	})
}

// 根据花销id编辑花销
export function deletExpenseApi({
	showLoading = true,
	expenseId
}) {
	return request({
		url: `/api/expense/deletExpense?expenseId=${expenseId}`,
		showLoading,
		method: 'delete'
	})
}
