import {
	request
} from "@/common/request.js"
/*
overview页面
/api/zeroox/zerooxProperty/overview
*/
/*
日程
*/


//获取房东查看日程的数据
export function getSchedulerDetailsList({
	showLoading = true,
	pageNum,
	pageSize,
	data
	
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/scheduler',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			data
		}
	})
}



//获取due_date_reminder页面数据
export function duDateReminder({
	showLoading = true,
	pageNum,
	pageSize,
	searchContent,
	propertyId
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/duDateReminder',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			searchContent,
			propertyId
		}
	})
}

//获取Property页面数据 /zeroox/zerooxProperty/properties

export function getProperties({
	showLoading = true,
	pageNum,
	pageSize,
	keyword
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/properties',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			keyword
		}
	})
}

//添加房子 
export function addProperties({
	showLoading = true,
	name,
	type,
	hmo,
	street,
	city,
	postcode,
	bedrooms,
	bathrooms,
	kitchen,
	livingrooms,
	notes
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/add',
		showLoading,
		method: 'post',
		data: {
			name,
			type,
			hmo,
			street,
			city,
			postcode,
			bedrooms,
			bathrooms,
			kitchen,
			livingrooms,
			notes
		}
	})
}
//查询房子信息/queryById
export function queryPropertyList({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/queryById',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//编辑房子信息
export function editPropertyList({
	showLoading = true,
	id,
	name,
	type,
	hmo,
	street,
	city,
	postcode,
	bedrooms,
	bathrooms,
	kitchen,
	livingrooms,
	notes
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/edit',
		showLoading,
		method: 'put',
		data: {
			id,
			name,
			type,
			hmo,
			street,
			city,
			postcode,
			bedrooms,
			bathrooms,
			kitchen,
			livingrooms,
			notes
		}
	})
}
//删除房子 /deleteBatch
export function deleteSingleProperties({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/delete?id=' + id,
		showLoading,
		method: 'delete'
	})
}
//获取Overview页面数据
export function getOverview({
	showLoading = true,
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/overview',
		showLoading,
		method: 'get'
	})
}
//添加提醒事务 /zeroox/zerooxPropertyTask/add
export function addTask({
	showLoading = true,
	propertyId,
	title,
	endTime,
	date,
	startTime,
	roomId
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyTask/add',
		showLoading,
		method: 'post',
		data: {
			propertyId,
			title,
			endTime,
			date,
			startTime,
			roomId
		}
	})
}
//删除任务 /api/zeroox/zerooxPropertyTask/delete

export function deleteTask({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxPropertyTask/delete?id=${id}`,
		showLoading,
		method: 'delete',
	})
}
//完成事务/zeroox/zerooxPropertyTask/markCompleted
export function markCompletedTask({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxPropertyTask/markCompleted`,
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}

//重新安排事务时间/zeroox/zerooxPropertyTask/reschedule
/*
private String taskId;
private Date startTime;
private Date date;
private Date endTime;
*/
export function rescheduleTask({
	showLoading = true,
	taskId,
	startTime,
	date,
	endTime
}) {
	return request({
		url: "/api/zeroox/zerooxPropertyTask/reschedule",
		showLoading,
		method: 'put',
		data: {
			taskId,
			startTime,
			date,
			endTime
		}
	})
}



//fix it  /zeroox/zerooxProperty/fixIt
export function getFixIt({
	showLoading = true,
	pageNum,
	pageSize,
	propertyId,
	status
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/fixIt',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			propertyId,
			status
		}
	})
}
//查询房间信息
export function selectRooms({
	showLoading = true,
	propertyId,
	roomType
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/selectRooms',
		showLoading,
		method: 'get',
		data: {
			propertyId,
			roomType
		}
	})
}
//批量删除房间/zeroox/zerooxRoom/deleteBatch
export function deleteBatch({
	showLoading = true,
	ids,
	porpertyId,
	roomType
}) {
	return request({
		url: `/api/zeroox/zerooxRoom/deleteBatch?ids=${ids}&porpertyId=${porpertyId}&roomType=${roomType}`,
		showLoading,
		method: 'DELETE',
	})
}
