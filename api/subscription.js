import {
	request
} from "@/common/request.js"
//获取下期账单
export function queryUserCardList({
	showLoading = true
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/queryUserCardList',
		showLoading,
		method: 'get'
	})
}
//移除卡片
export function deleteUserCard({
	showLoading = true,
	data = {
		cardId
	}
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/deleteUserCard',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//更新卡片/api/zeroox/zerooxUserSubscription/PutUserCardList
export function PutUserCardList({
	showLoading = true,
	data = {
		cardId,
		cardName,
		cardAddresline1,
		cardAddresline2,
		cardMonth,
		cardYear,
		cardPostcode,
		cardCountry,
		cardCity
	}
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/PutUserCardList',
		showLoading,
		method: 'put',
		data: {
			...data
		}
	})
}
//获取下期账单
export function getLatestInvoice({
	showLoading = true
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/getLatestInvoiceUser',
		showLoading,
		method: 'get'
	})
}
//获取历史账单列表
export function getInvoiceList({
	showLoading = true,
	data = {
		limit
	}
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/getInvoiceList',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//更新订阅
export function calculateThePrice({
	showLoading = true,
	data = {
		num
	}
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/CalculateThePrice',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//获取当前订阅信息
export function getSubscription({
	showLoading = true
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/getSubscription',
		showLoading,
		method: 'get'
	})
}

//取消订阅
export function cancelSubscription({
	showLoading = true
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/cancelSubscription',
		showLoading,
		method: 'get'
	})
}
//激活订阅
export function updatadTenancyStatus({
	showLoading = true,
	tenancyId
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/updatadTenancyStatus',
		showLoading,
		method: 'put',
		data:{
			tenancyId
		}
	})
}
//创建订阅
export function createSubscription({
	showLoading = true,
	data = {
		tenancyCount,
		paymentPeriod: 'Monthly'
	}
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/createSubscriptionUser',
		showLoading,
		method: 'get',
		// header: {
		// 	'content-type': 'multipart/form-data'
		// },
		data: {
			...data
		}
	})
}
//更新订阅
export function updateSubscription({
	showLoading = true,
	data = {
		tenancyCount,
		paymentPeriod,
		tenancyId
	}
}) {
	return request({
		url: '/api/zeroox/zerooxUserSubscription/updateSubscription',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}
//判断是否订阅过期或续费失败
export function getFailState({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/function',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}