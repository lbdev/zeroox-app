/*
新增保险，租约
*/
import {
	request
} from "@/common/request.js"
/*
--------房子----------
*/

// Tenancy房子 zeroox/zerooxTenancy/propertyList
export function propertyList({
	showLoading = true,
	pageNum,
	pageSize,
	searchContent
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/propertyList',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			searchContent
		}
	})
}
//房间列表 /zeroox/zerooxTenancy/roomList
export function roomList({
	showLoading = true,
	pageNum,
	pageSize,
	searchContent,
	propertyId,
	type
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/roomList',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			searchContent,
			propertyId,
			type
		}
	})
}
//zerooxPropertyUtilites 房子列表
export function propertyUtilitesList({
	showLoading = true,
	pageNum,
	pageSize,
	searchContent
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyUtilites/propertyList',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			searchContent
		}
	})
}

//查询房子信息 /zeroox/zerooxProperty/propertyList
export function duePropertyList({
	showLoading = true,
}) {
	return request({
		url: '/api/zeroox/zerooxProperty/propertyList',
		showLoading,
		method: 'get',
	})
}
/*
----------租约
*/

//1.add a tenancy页面，新增租约 /zeroox/zerooxTenancy/add
export function addTenancy({
	showLoading = true,
	zerooxTenants,
	sendInviteMessage,
	type,
	rent,
	paymentPeriod,
	startDate,
	endDate,
	runsOnAsPeriodic,
	breakClause,
	depositScheme,
	deposit,
	agreement,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/add',
		showLoading,
		method: 'post',
		data: {
			zerooxTenants,
			sendInviteMessage,
			type,
			rent,
			paymentPeriod,
			startDate,
			endDate,
			runsOnAsPeriodic,
			breakClause,
			depositScheme,
			deposit,
			agreement,
			fileSize
		}
	})
}

//2.编辑租约	/edit
export function editTenancy({
	showLoading = true,
	tenantAndPropertyAndRoomList,
	sendInviteMessage,
	type,
	rent,
	paymentPeriod,
	startDate,
	endDate,
	runsOnAsPeriodic,
	breakClause,
	depositScheme,
	deposit,
	agreement,
	id,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/edit',
		showLoading,
		method: 'put',
		data: {
			tenantAndPropertyAndRoomList,
			sendInviteMessage,
			type,
			rent,
			paymentPeriod,
			startDate,
			endDate,
			runsOnAsPeriodic,
			breakClause,
			depositScheme,
			deposit,
			agreement,
			id,
			fileSize
		}
	})
}



//3.租约列表 /tenancies
export function getTenanciesList({
	showLoading = true,
	pageNum,
	pageSize,
	searchContent,
	isDemotion
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/tenancies',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			searchContent,
			isDemotion
		}
	})
}


//4.租约 是否接收到期提醒消息
export function updateNotification({
	showLoading = true,
	id, //租约id
	isNotification //是否到期通知
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/updateNotification?id=${id}&isNotification=${isNotification}`,
		showLoading,
		method: 'put'
	})
}


//5.根据房子id查询租约列表 getByPropertyIdTenancies
export function getByPropertyIdTenancies({
	showLoading = true,
	propertyId,
	pageNum,
	pageSize,
	searchContent
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/getByPropertyIdTenancies',
		showLoading,
		method: 'get',
		data: {
			propertyId,
			pageNum,
			pageSize,
			searchContent
		}
	})
}

//6.根据租约id查询租约信息/queryById
export function getQueryByIdTenancies({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxTenancy/queryById',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//7,租客身份---查看我的租约信息/myTenancy
export function queryMyTenancy({
	showLoading = true,
	content
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/myTenancy?content=${content}`,
		showLoading,
		method: 'post',
	})
}
//8.房东身份----查看租约信息 zeroox/zerooxTenancy/tenancy
export function queryTenancy({
	showLoading = true,
	tenancyId,
	content
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/tenancy?tenancyId=${tenancyId}&content=${content}`,
		showLoading,
		method: 'post',
	})
}
//9.删除租约
export function deleteTenancy({
	showLoading = true,
	// tenancyId
	id
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/delete?id=${id}`,
		showLoading,
		method: 'delete',
	})
}
//查看是否可以添加租约 /isTenancy
export function isTenancy({
	showLoading = true,
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/isTenancy`,
		showLoading,
		method: 'get'
	})
}
//判断输入的邮箱是否注册/zeroox/zerooxTenancy/isTenantUser

export function isTenantUser({
	showLoading = true,
	email
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/isTenantUser`,
		showLoading,
		method: 'get',
		data: {
			email
		}
	})
}

/*---------------------电费*/
//add a energy页面 /zeroox/zerooxPropertyUtilites/add
export function addEnergy({
	showLoading = true,
	type,
	category,
	endDate,
	propertyId,
	cost,
	paymentPeriod,
	notify,
	images,
	provider,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyUtilites/add',
		showLoading,
		method: 'post',
		data: {
			type,
			category,
			endDate,
			propertyId,
			cost,
			paymentPeriod,
			notify,
			images,
			provider,
			fileSize
		}
	})
}
//Utilities页面 /zeroox/zerooxPropertyUtilites/utilities
export function utilities({
	showLoading = true,
	propertyId,
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyUtilites/utilities',
		showLoading,
		method: 'get',
		data: {
			propertyId,
		}
	})
}
//更新电费通知
export function utilitesNotification({
	showLoading = true,
	id, //租约id
	isNotification //是否到期通知
}) {
	return request({
		url: `/api/zeroox/zerooxPropertyUtilites/updateNotifi?id=${id}&isNotification=${isNotification}`,
		showLoading,
		method: 'get'
	})
}

/*
	根据房子id查询电费 /zeroox/zerooxPropertyUtilites/queryById
*/
export function queryByIdUtilities({
	showLoading = true,
	id,
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyUtilites/queryById',
		showLoading,
		method: 'get',
		data: {
			id,
		}
	})
}
/**
 修改电费信息 /zeroox/zerooxPropertyUtilites/edit
**/
export function editUtilities({
	showLoading = true,
	id,
	category,
	type,
	endDate,
	propertyId,
	provider,
	cost,
	paymentPeriod,
	notify,
	images,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyUtilites/edit',
		showLoading,
		method: 'put',
		data: {
			id,
			category,
			type,
			endDate,
			propertyId,
			provider,
			cost,
			paymentPeriod,
			notify,
			images,
			fileSize
		}
	})
}

//删除电费信息
export function deleteUtilities({
	showLoading = true,
	id,
}) {
	return request({
		url: `/api/zeroox/zerooxPropertyUtilites/delete?id=${id}`,
		showLoading,
		method: 'DELETE',
		// data: {
		// 	id,
		// }
	})
}

//  保险公司名单
export function getProvider({
	showLoading = true,
	provider
}) {
	return request({
		url: '/api/zeroox/zerooxInsurance/provider',
		showLoading,
		method: 'get',
		data: {
			provider
		}
	})
}
//根据id查询证书
export function queryInsurance({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxInsurance/queryById',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//根据id删除证书
export function deleteInsurance({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxInsurance/delete?id=${id}`,
		showLoading,
		method: 'delete',
	})
}

//查询保险信息

//更新保险通知
export function insuranceNotification({
	showLoading = true,
	id, //租约id
	isNotification //是否到期通知
}) {
	return request({
		url: `/api/zeroox/zerooxInsurance/updateNotifi?id=${id}&isNotification=${isNotification}`,
		showLoading,
		method: 'get'
	})
}
// 添加保险页面      /zeroox/zerooxInsurance/add
export function addInsurance({
	showLoading = true,
	type,
	endDate,
	propertyId,
	provider,
	cost,
	paymentPeriod,
	notify
}) {
	return request({
		url: '/api/zeroox/zerooxInsurance/add',
		showLoading,
		method: 'post',
		data: {
			type,
			endDate,
			propertyId,
			provider,
			cost,
			paymentPeriod,
			notify
		}
	})
}
//更新保险页面      /zeroox/zerooxInsurance/add
export function editInsurance({
	showLoading = true,
	type,
	endDate,
	propertyId,
	provider,
	cost,
	paymentPeriod,
	notify,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxInsurance/edit',
		showLoading,
		method: 'put',
		data: {
			type,
			endDate,
			propertyId,
			provider,
			cost,
			paymentPeriod,
			notify,
			id
		}
	})
}

//  获取可选择电力公司的列表
export function getCategory({
	showLoading = true,
	name
}) {
	return request({
		url: '/api/zeroox/zerooxPropertyUtilites/category',
		showLoading,
		method: 'get',
		data: {
			name
		}
	})
}
/*
--------------------------------证件

*/

//获取证件类型 /zeroox/zerooxCertificate/certificateType
export function getCertificateType({
	showLoading = true,
	certificateType
}) {
	return request({
		url: '/api/zeroox/zerooxCertificate/certificateType',
		showLoading,
		method: 'get',
		data: {
			certificateType
		}
	})
}

//更新证件通知
export function certificateNotification({
	showLoading = true,
	id, //租约id
	isNotification //是否到期通知
}) {
	return request({
		url: `/api/zeroox/zerooxCertificate/updateNotifi?id=${id}&isNotification=${isNotification}`,
		showLoading,
		method: 'get'
	})
}
//添加证件 /zeroox/zerooxCertificate/add

export function addCertificate({
	showLoading = true,
	type,
	expiryDate,
	propertyId,
	notify,
	publicView,
	files,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxCertificate/add',
		showLoading,
		method: 'post',
		data: {
			type,
			expiryDate,
			propertyId,
			notify,
			publicView,
			files,
			fileSize
		}
	})
}
//添加证件 /zeroox/zerooxCertificate/edit

export function editCertificate({
	showLoading = true,
	type,
	expiryDate,
	propertyId,
	notify,
	publicView,
	files,
	id,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxCertificate/edit',
		showLoading,
		method: 'put',
		data: {
			type,
			expiryDate,
			propertyId,
			notify,
			publicView,
			files,
			id,
			fileSize
		}
	})
}
//根据id查询证书
export function queryCertificate({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxCertificate/queryById',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//根据id删除证书
export function deleteCertificate({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxCertificate/delete?id=${id}`,
		showLoading,
		method: 'delete',
	})
}
//获取房子中的证件信息/zeroox/zerooxCertificate/seafetyCeertIns
export function seafetyCeertIns({
	showLoading = true,
	propertyId,
	seafe
}) {
	return request({
		url: '/api/zeroox/zerooxCertificate/seafetyCeertIns',
		showLoading,
		method: 'get',
		data: {
			propertyId,
			seafe
		}
	})
}
//首页进入获取证件信息
export function certificatesAndInsurance({
	showLoading = true,
	seafe
}) {
	return request({
		url: '/api/zeroox/zerooxCertificate/certificatesAndInsurance',
		showLoading,
		method: 'get',
		data: {
			seafe
		}
	})
}
/*
房间管理------------------ /zeroox/zerooxRoom/getPropertyIdRoomAndAssets
*/
export function getPropertyIdRoomAndAssets({
	showLoading = true,
	propertyId,
}) {
	return request({
		url: '/api/zeroox/zerooxRoom/getPropertyIdRoomAndAssets',
		showLoading,
		method: 'get',
		data: {
			propertyId,
		}
	})
}

//添加房间
export function addRoomAndAssets({
	showLoading = true,
	propertyId,
	name
}) {
	return request({
		url: '/api/zeroox/zerooxRoom/add',
		showLoading,
		method: 'post',
		data: {
			propertyId,
			name
		}
	})
}

//获取添加电器的类型	/zeroox/zerooxAssets/appliancAndFurniture
export function appliancAndFurniture({
	showLoading = true,
	name
}) {
	return request({
		url: '/api/zeroox/zerooxAssets/appliancAndFurniture',
		showLoading,
		method: 'get',
		data: {
			name
		}
	})
}
//获取电器列表
export function assetsRooms({
	showLoading = true,
	propertyId,
	roomName
}) {
	return request({
		url: '/api/zeroox/zerooxAssets/assetsRooms',
		showLoading,
		method: 'get',
		data:{
			propertyId,
			roomName
		}
	})
}
//查询电器信息/queryById
export function queryAssetsId({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxAssets/queryById',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}


//用户可选择离到期日天数 /GetNotified typeName
export function getNotified({
	showLoading = true,
	typeName
}) {
	return request({
		url: '/api/zeroox/zerooxAssets/GetNotified',
		showLoading,
		method: 'get',
		data: {
			typeName
		}
	})
}

//添加电器
export function addNotified({
	showLoading = true,
	type,
	name,
	propertyId,
	roomId,
	lifetimeWaranty,
	warrantyExpiryDate,
	notifyType,
	make,
	mode,
	width,
	height,
	depth,
	unit,
	purchasePrice,
	purchaseDate,
	images,
	pathType,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxAssets/add',
		showLoading,
		method: 'post',
		data: {
			type,
			name,
			propertyId,
			roomId,
			lifetimeWaranty,
			warrantyExpiryDate,
			notifyType,
			make,
			mode,
			width,
			height,
			depth,
			unit,
			purchasePrice,
			purchaseDate,
			images,
			pathType,
			fileSize
		}
	})
}

//编辑电器/edit
export function editNotified({
	showLoading = true,
	id,
	type,
	name,
	propertyId,
	roomId,
	lifetimeWaranty,
	warrantyExpiryDate,
	notifyType,
	make,
	mode,
	width,
	height,
	depth,
	unit,
	purchasePrice,
	purchaseDate,
	images,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxAssets/edit',
		showLoading,
		method: 'put',
		data: {
			id,
			type,
			name,
			propertyId,
			roomId,
			lifetimeWaranty,
			warrantyExpiryDate,
			notifyType,
			make,
			mode,
			width,
			height,
			depth,
			unit,
			purchasePrice,
			purchaseDate,
			images,
			fileSize
		}
	})
}
//删除电器
export function deleteNotified({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxAssets/delete?id=${id}`,
		showLoading,
		method: 'delete',
	})
}
/*
维修人员管理
*/
//1.添加维修人员 /zeroox/zerooxContractor/add
export function addContractor({
	showLoading = true,
	name,
	email,
	mobile,
	propertyId,
	skills,
	address,
	notes,
	company,
	images,
	regionCode,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxContractor/add',
		showLoading,
		method: 'post',
		data: {
			name,
			email,
			mobile,
			propertyId,
			skills,
			address,
			notes,
			company,
			images,
			regionCode,
			fileSize
		}
	})
}
//2.维修人员技巧列表 /zeroox/zerooxContractor/skill
export function getSkill({
	showLoading = true,
	parameter
}) {
	return request({
		url: '/api/zeroox/zerooxContractor/skill',
		showLoading,
		method: 'get',
		data: {
			parameter
		}
	})
}
//3.获取维修人员列表 /favContractors
export function getFavContractors({
	showLoading = true,
	pageNum,
	pageSize,
	keyword,
	propertyId
}) {
	return request({
		url: '/api/zeroox/zerooxContractor/favContractors',
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			keyword,
			propertyId
		}
	})
}
//4.获取维修人员信息
export function getContractor({
	showLoading = true,
	id
}) {
	return request({
		url: '/api/zeroox/zerooxContractor/contractor',
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//5.删除维修人员
export function deleteContractor({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxContractor/delete?id=${id}`,
		showLoading,
		method: 'delete',
	})
}
//5.查询维修人员
export function queryContractor({
	showLoading = true,
	id
}) {
	return request({
		url: `/api/zeroox/zerooxContractor/queryById`,
		showLoading,
		method: 'get',
		data: {
			id
		}
	})
}
//5.编辑维修人员
export function editContractor({
	showLoading = true,
	id,
	name,
	email,
	mobile,
	propertyId,
	skills,
	address,
	notes,
	company,
	images,
	regionCode,
	fileSize
}) {
	return request({
		url: '/api/zeroox/zerooxContractor/edit',
		showLoading,
		method: 'PUT',
		data: {
			id,
			name,
			email,
			mobile,
			propertyId,
			skills,
			address,
			notes,
			company,
			images,
			regionCode,
			fileSize
		}
	})
}
/*
租客/文件管理

*/
//1.获取文件/zeroox/zerooxTenancy/files

export function getFiles({
	showLoading = true,
	pageNum,
	pageSize,
	searchContent
}) {
	return request({
		url: `/api/zeroox/zerooxTenancy/files`,
		showLoading,
		method: 'get',
		data: {
			pageNum,
			pageSize,
			searchContent
		}
	})
}
// //2.获取 租约/文件模块 /api/zeroox/zerooxTenancy/myTenancy
// export function getMyTenancy({
// 	showLoading = true,
// 	tenancyId,
// 	content
// }) {
// 	return request({
// 		url: `/api/zeroox/zerooxTenancy/myTenancy`,
// 		showLoading,
// 		method: 'get',
// 		data: {
// 			pageNum,
// 			tenancyId,
// 			content
// 		}
// 	})
// }


//更新家具通知
export function furnitureNotification({
	showLoading = true,
	id, //租约id
	isNotification //是否到期通知
}) {
	return request({
		url: `/api/zeroox/zerooxAssets/updateNotification?id=${id}&isNotification=${isNotification}`,
		showLoading,
		method: 'put'
	})
}