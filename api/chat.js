import {
	request
} from "@/common/request.js"


// 通过传target（邮箱）获取后台Token
export function createConversation({
	showLoading = true,
	data = {
		target 
	}
}) {
	return request({
		url: '/api/chat/twilio/createConversation',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}

// 获取所有租户列表信息
export function getConversationList({
	showLoading = true,
	data = {
		propertyId: null,
		contactName: null,
		propertyName: null,
		roomName: null,
		pageNum: 1,
		pageSize: 10,
		keyword:null
	}
}) {
	return request({
		url: '/api/chat/twilio/getConversationList',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}



// 更新对话最新活跃时间
export function updateLatestActiveTime({
	showLoading = true,
	data = {
		sid
	}
}) {
	return request({
		url: '/api/chat/twilio/updateLatestActiveTime',
		showLoading,
		method: 'get',
		data: {
			...data
		}
	})
}

// 用户点击后可输入租客名称或房子名称，搜索相关的信息。
// export function selectByName({
// 	showLoading = true,
// 	keyWord
// }) {
// 	return request({
// 		url: '/api/chat/twilio/selectByName',
// 		showLoading,
// 		method: 'get',
// 		data: {
// 			keyWord
// 		}
// 	})
// }

//根据当前登陆者的不同身份和用户进行聊天，看到的是聊天用户信息
export function getTarget({
	showLoading = true,
	target
}) {
	return request({
		url: '/api/chat/twilio/getTarget',
		showLoading,
		method: 'get',
		data: {
			target
		}
	})
}

//判断是否打开聊天框
export function getIsChatting({
	showLoading = true,
	num
}){
	return request({
		url: '/api/chat/twilio/isChatting',
		showLoading,
		method: 'get',
		data: {
			num
		}
	})
}





