import App from './App'
import store from './store'
import baseUrl from "@/common/baseUrl.js"
import {
	copy,
	browser
} from "@/common/util.js"
import h5Copy from '@/js_sdk/junyi-h5-copy/junyi-h5-copy.js'
// #ifndef VUE3
import Vue from 'vue'
import {
	getFailState
} from '@/api/subscription.js'
import moment from 'moment'
Vue.prototype.$formatTime = (mills, fmt = 'YYYY-MM-DD HH:mm:ss') => {
	return moment(mills).format(fmt)
}
Vue.prototype.$moment = moment
Vue.prototype.$countdownTime = 1 * 180 * 1000
import uView from "uview-ui";
Vue.use(uView);
console.log(uni.$u.config.v);
uni.$u.setConfig({
	props: {
		modal: {
			confirmColor: '#67A5CE',
			cancelColor: '#000'
		}
	}
})

Vue.config.productionTip = false
Vue.prototype.$store = store
App.mpType = 'app'
import i18n from "@/lang/index.js"
const app = new Vue({
	i18n,
	store,
	...App
})
app.$mount()
Vue.prototype.$updateTabBar = () => {
	// uni.setTabBarItem({
	// 	index: 0,
	// 	text: i18n.t('tabar.home'),
	// })
	// uni.setTabBarItem({
	// 	index: 1,
	// 	text: i18n.t('tabar.earn'),
	// })
	// // uni.setTabBarItem({
	// // 	index: 2,
	// // 	text: i18n.t('tabar.team'),
	// // })
	// uni.setTabBarItem({
	// 	index: 2,
	// 	text: i18n.t('tabar.market'),
	// })
	// uni.setTabBarItem({
	// 	index: 3,
	// 	text: i18n.t('tabar.mine'),
	// })
}
// #endif

//切换用户模块 存用户信息
Vue.prototype.addUserInfo = function() {
	var userList = uni.getStorageSync('userList')
	if (!userList) { //空
		userList = [{
			...store.state.user.info
		}]
	} else {
		var exists = -1
		for (var i = 0; i < userList.length; i++) {
			let u = userList[i]
			if (u.username == store.state.user.info.username) {
				userList[i] = Object.assign(u, store.state.user.info)
				exists = i
				break;
			}
		}
		console.log('exists', exists)
		if (exists == -1) {
			userList.unshift({
				...store.state.user.info
			})
		} else if (exists != 0) {
			userList.unshift(userList.splice(exists, 1)[0])
		}
	}
	// console.log('用户列表',userList)
	uni.setStorageSync('userList', userList);

}
//跳转到Privacy policy页面
Vue.prototype.toPrivacyPolicy = function() {
	uni.navigateTo({
		url: `/pages/index/clause?title=Privacy Policy`
	})
}
//跳转到Terms and Conditions页面
Vue.prototype.toTCs = function() {
	uni.navigateTo({
		url: `/pages/index/clause?title=TCs`
	})
}
const getOAID = function() {
	return new Promise((resolve, reject) => {
		plus.device.getOAID({
			success: (res) => {
				console.log('getOAID', res)
				resolve(res.oaid)
			},
			fail: (err) => {
				console.error('getInfo', err)
				resolve()
			}
		});
	})
}
const getInfo = function() {
	return new Promise((resolve, reject) => {
		plus.device.getInfo({
			success: (res) => {
				console.log('getInfo', res)
				resolve(res)
			},
			fail: (err) => {
				console.error('getInfo', err)
				resolve({})
			}
		})
	})
}
Vue.prototype.$sysInfo = uni.getSystemInfoSync()
Vue.prototype.$getDeviceId = async function() {
	let sysInfo = uni.getSystemInfoSync()
	// #ifdef APP
	var {
		imei,
		imsi,
		uuid,
		idfa
	} = await getInfo()
	if (sysInfo.platform == "android") {
		let oaid = await getOAID()
		if (imsi && imsi.length) {
			imsi = imsi[0]
		}
		return imei || oaid || imsi || uuid || sysInfo.deviceId
	} else if (sysInfo.platform == "ios") {
		return idfa || uuid
	}
	// #endif
	return sysInfo.deviceId
}

Vue.prototype.$copy = ({
	text
}) => {
	// #ifdef H5
	const result = h5Copy(text)
	if (!result) {
		uni.showToast({
			title: '浏览器不支持复制，请手动复制',
			icon: 'none',
			duration: 1500
		})
	} else {
		uni.showToast({
			title: i18n.t('common.copySuccess'),
			icon: 'none'
		})
	}
	// #endif 
	// #ifndef H5
	uni.setClipboardData({
		data: text,
		showToast: false,
		success: () => {
			uni.showToast({
				title: i18n.t('common.copySuccess'),
				icon: 'none'
			})
		}
	})
	// #endif
}

Vue.prototype.$browser = browser

Vue.prototype.$showError = (msg) => {
	uni.showToast({
		title: msg,
		duration: 3000,
		icon: "none"
	})
}

//#ifdef APP-PLUS
Vue.prototype.$openMarket = () => {
	var appurl;
	if (plus.os.name == "Android") {
		appurl = "market://details?id=net.ezeloan.in";
	} else {
		appurl = "itms-apps://itunes.apple.com/cn/app/ezeloan-app/id1417078253?mt=8";
	}
	plus.runtime.openURL(appurl, function(res) {
		console.log(res);
	});
}
//#endif

Vue.prototype.$toast = (msg) => {
	return new Promise((resolve => {
		uni.showToast({
			title: msg,
			duration: 1500,
			icon: "none"
		})
		setTimeout(() => {
			resolve()
		}, 1500)
	}))
}
import loading from "@/common/custom-loading.js"
Vue.prototype.$loading = loading

let sysInfo = uni.getSystemInfoSync()
Vue.prototype.$bigScreen = sysInfo.windowWidth > 960
if (sysInfo.platform == 'ios') {
	Vue.prototype.$customNavHeight = sysInfo.statusBarHeight + 44
} else if (sysInfo.platform == 'android') {
	Vue.prototype.$customNavHeight = sysInfo.statusBarHeight + 44
}

Vue.prototype.$getFileUrl = (fileName) => {
	if (!fileName) {
		return ''
	} else if (fileName.startsWith('http')) {
		return fileName
	}
	return `${baseUrl.apiBaseUrl}/api/digitalocean/getFile?fileName=${fileName}`
}

Vue.prototype.$getFileKey = (fileUrl) => {
	return fileUrl.replace(`${baseUrl.apiBaseUrl}/api/digitalocean/getFile?fileName=`, '')
}

Vue.prototype.$getCaptchaUrl = () => {
	return `${baseUrl.apiBaseUrl}/login/captcha`
}
Vue.prototype.$getLeaseState = async function(userId) {
	try {
		if (store.state.user.info.userType === 2) {
			console.log('房东请求接收到的对面聊天用户id',userId);
			let res = await getFailState({
				id: userId
			})
			console.log('房东请求', res)
			return res
		} else if (store.state.user.info.userType === 3) {
			let res = await getFailState({})
			console.log('租客请求', res)
			return res
		}

	} catch (e) {
		console.error(e)
	}
}
// #ifdef VUE3
import {
	createSSRApp
} from 'vue'

export function createApp() {
	const app = createSSRApp(App)
	app.use(store)
	return {
		app
	}
}
// #endif
